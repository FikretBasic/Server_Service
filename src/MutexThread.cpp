/*
 * MutexThread.cpp
 *
 *  Created on: 7 Feb 2018
 *      Author: its2016
 */

#include "MutexThread.h"


#if defined(__linux__)

MutexThread::MutexThread()
{
	if(pthread_mutex_init(&lock, NULL) != 0)
	{
		perror("Mutex init: ");
		exit(1);
	}
}

MutexThread::~MutexThread()
{
	if(pthread_mutex_destroy(&lock) != 0)
	{
		perror("Mutex destroy: ");
		exit(1);
	}
}

int MutexThread::acquire()
{
	return pthread_mutex_lock(&lock);
}

int MutexThread::release()
{
	return pthread_mutex_unlock(&lock);
}

#elif defined(_WIN32)

MutexThread::MutexThread()
{
	InitializeCriticalSection(&lock);
}

MutexThread::~MutexThread()
{
	DeleteCriticalSection (&lock);
}

int MutexThread::acquire()
{
	EnterCriticalSection (&lock);
	return 0;
}

int MutexThread::release()
{
	LeaveCriticalSection (&lock);
	return 0;
}

#elif defined(__sun)

MutexThread::MutexThread()
{
	if(mutex_init(&lock, NULL) != 0)
	{
		perror("Mutex init: ");
		exit(1);
	}
}

MutexThread::~MutexThread()
{
	if(mutex_destroy(&lock) != 0)
	{
		perror("Mutex destroy: ");
		exit(1);
	}
}

int MutexThread::acquire()
{
	return mutex_lock(&lock);
}

int MutexThread::release()
{
	return mutex_unlock(&lock);
}

#endif


