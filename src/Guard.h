/*
 * Guard.h
 *
 *  Created on: 8 Feb 2018
 *      Author: its2016
 */

#ifndef GUARD_H_
#define GUARD_H_

/**
 * \brief Guard class for enabling C++ Scoped Locking which ensures that a lock is acquired when the controller
 * enters the scope and released when the controller leaves the scope.
 */

template <class LOCK>
class Guard
{
public:
	/**
	 * \brief Initialisation constructor for Guard class. It is called during the definition of the scope.
	 * \param lck - specific lock
	 */
	Guard (LOCK &lck): lock (lck)
	{
		lock.acquire();
	}

	/**
	 * Destructor of the guard class. Calls release of the lock control.
	 */
	~Guard()
	{
		lock.release();
	}

private:
	LOCK &lock;		/**< Variable used to for the control of the locking process */
};


#endif /* GUARD_H_ */
