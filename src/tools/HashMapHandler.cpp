/*
 * HashMapHandler.cpp
 *
 *  Created on: 1 Mar 2018
 *      Author: its2016
 */

#include "HashMapHandler.h"

HashMapHandler* HashMapHandler::instance_t = 0;
HashMap<int,std::string>* HashMapHandler::ipAddrHashMap = 0;

HashMapHandler::HashMapHandler()
{
	ipAddrHashMap = new HashMap<int, std::string>;
}

HashMapHandler::~HashMapHandler()
{
	delete this->instance_t;
	delete this->ipAddrHashMap;
}

HashMapHandler* HashMapHandler::instance()
{
    if (!instance_t)
    	instance_t = new HashMapHandler();
    return instance_t;
}

HashMap<int, std::string>* HashMapHandler::getIpAddrHashMap()
{
	return ipAddrHashMap;
}





