/*
 * SimplePacket.h
 *
 *  Created on: 16 Mar 2018
 *      Author: its2016
 */

#include "../Includes.h"

#ifndef TOOLS_SIMPLEPACKET_H_
#define TOOLS_SIMPLEPACKET_H_

class SimplePacket
{
public:
	SimplePacket();
	SimplePacket(char* message);

	virtual ~SimplePacket();

	void setMessage(char *message);

	std::string getType();
	std::string getData();

private:
	std::string type;
	std::string data;
};
#endif /* TOOLS_SIMPLEPACKET_H_ */
