/*
 * HelperFunctions.cpp
 *
 *  Created on: 8 Mar 2018
 *      Author: its2016
 */

#include "HelperFunctions.h"

#if defined (_WIN32)

std::string getCurrentTime()
{
	SYSTEMTIME lt;
	GetLocalTime(&lt);
	std::string timeString = Str(lt.wHour) + ":" + Str(lt.wMinute) + ":" + Str(lt.wSecond);

	return timeString;
}

#else

const char* getCurrentTime()
{
	time_t currentTime;
	struct tm *localTime_ = NULL;
	time(&currentTime);
	localTime_ = localtime(&currentTime);

	std::string timeString = Str(localTime_->tm_hour) + ":" + Str(localTime_->tm_min) + ":" + Str(localTime_->tm_sec);
	return timeString.c_str();
}

std::string get_str_between_two_str(const std::string &s,
        const std::string &start_delim,
        const std::string &stop_delim)
{
    unsigned first_delim_pos = s.find(start_delim);
    unsigned end_pos_of_first_delim = first_delim_pos + start_delim.length();
    unsigned last_delim_pos = s.find(stop_delim);

    return s.substr(end_pos_of_first_delim,
            last_delim_pos - end_pos_of_first_delim);
}

#endif
