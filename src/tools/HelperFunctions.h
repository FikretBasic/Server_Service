/*
 * HelperFunctions.h
 *
 *  Created on: 8 Mar 2018
 *      Author: its2016
 */

#include "../Includes.h"

#ifndef TOOLS_HELPERFUNCTIONS_H_
#define TOOLS_HELPERFUNCTIONS_H_

#if defined(_WIN32)
std::string getCurrentTime();
#else
 /**
 * \brief Get current system time (timestamp) in HH:MM:SS format
 * \return const char* - char array which holds the current time
 */
const char* getCurrentTime();
#endif

std::string get_str_between_two_str(const std::string &s,
        const std::string &start_delim,
        const std::string &stop_delim);

#endif /* TOOLS_HELPERFUNCTIONS_H_ */
