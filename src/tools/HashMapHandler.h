/*
 * HashMapHandler.h
 *
 *  Created on: 1 Mar 2018
 *      Author: its2016
 */

#include "HashMap.h"
#include "../Includes.h"

#ifndef TOOLS_HASHMAPHANDLER_H_
#define TOOLS_HASHMAPHANDLER_H_

/**
 * \brief Singleton class for enabling Hash functionality
 */

class HashMapHandler {
public:

	/**
	 * \brief Destructor of the class (deletes specific private variables)
	 */
	virtual ~HashMapHandler();

	/**
	 * \brief Get (or create) specific class instance
	 * \return HashMapHandler* - pointer to the class instance
	 */
	static HashMapHandler* instance();

	/**
	 * \brief Get pointer of the IP Address HashMap object
	 * \return HashMap<int, std::string>* - HashMap pointer
	 */
	static HashMap<int, std::string>* getIpAddrHashMap();

private:
	/**
	 * \brief Constructor of the class, hidden to only allow inner class calls
	 */
	HashMapHandler();

	static HashMapHandler *instance_t;					/**< Instance of the class */
	static HashMap<int, std::string> *ipAddrHashMap;	/**< Instance of the specific IP address HashMap  */
};

#endif /* TOOLS_HASHMAPHANDLER_H_ */
