/*
 * SOCKETHandler.h
 *
 *  Created on: 7 Feb 2018
 *      Author: its2016
 */

/// \todo [DONE - 08.03.2018.] Include the Strategy Design Pattern or similar to handle different server activities.

#include "SOCKETConnection.h"
#include "SOCKETCommunication.h"
#include "../ThreadHandler.h"
#include "../Guard.h"
#include "../MutexThread.h"
#include "../Includes.h"
#include "strategy/SOCKETActivity.h"
#include "strategy/FORKAction.h"
#include "strategy/THREADAction.h"

#ifndef SOCKETHANDLER_H_
#define SOCKETHANDLER_H_

namespace server {

/**
 * \brief Handles a particular Socket communication (Server - Client)
 */

class SOCKET_Handler
{
public:

	/**
	 * \brief Initialising constructor which handles a particular socket connection and communication
	 * \param server_socc - socket connection object instance
	 * \param stream_comm - socket communication object instance
	 */
	SOCKET_Handler(SOCKET_Connection &server_socc, SOCKET_Communication &stream_comm);

	/**
	 * \brief Empty destructor, connection and communication automatically close the connection from their side
	 */
	virtual ~SOCKET_Handler();

	/**
	 * \brief Main function to start the interaction between the Server and the Client
	 */
	void startActivity();

	/**
	 * \brief Set a particular type of the Socket multi-client handling (Thread, Fork, ...)
	 * \param act_type - handling type
	 */
	void setActivity(const char* act_type);

private:
	SOCKET_Connection server_socc;		/**< Instance of the Socket connection */
	SOCKET_Communication stream_comm;	/**< Instance of the Socket communication */

	SOCKET_Activity *sock_act;		/**< Particular type of the Socket activity, interface using the Strategy pattern */

	/**
	 * \brief Empty constructor, to hide from the possibility to initialise it outside
	 */
	SOCKET_Handler();
};

} /* namespace server */

#endif /* SOCKETHANDLER_H_ */
