/*
 * SOCKETConnection.cpp
 *
 *  Created on: 7 Feb 2018
 *      Author: its2016
 */

#include "SOCKETConnection.h"

namespace server {

SOCKET_Connection::SOCKET_Connection()
{
	this->socket = -1;
}

SOCKET_Connection::SOCKET_Connection(NET_Addr *address, int pendingConnections)
{
	struct addrinfo *iter;

	for (iter = address->getServerInfo(); iter != NULL; iter = iter->ai_next)
	{
		this->socket = socketConnect(iter, 0);
		if(this->socket == -1)
			continue;

		socketOptReusePort(this->socket);

		if(socketBind(this->socket, iter, 0) == -1)
			continue;

		this->printServerInfo(iter);

		break;
	}

	checkAndTerminate(iter);

	if (pendingConnections <= 0)
	{
		socketListen(this->socket, 10, 1);
	}
	else
	{
		socketListen(this->socket, pendingConnections, 1);
	}
}

SOCKET_Connection::~SOCKET_Connection()
{
	closeSocket(this->socket);
	#if defined(_WIN32)
		WSACleanup();
	#endif
}

/// \todo [DONE - 01.03.2018.] Finish cleaning up the function and the output which is desired.

int SOCKET_Connection::acceptClient(SOCKET_Communication &stream)
{
	struct sockaddr_storage *clientAddr = new struct sockaddr_storage();

	SOCKET clientSockID = acceptNewClient(this->socket, clientAddr);
	stream.setSocket(clientSockID);

	if (stream.getSocket() == -1)
	{
		std::string sockMess = "[-] [" + Str(getCurrentTime()) + "] Issue connecting a new Client";
		Stylist::instance()->printMessage(sockMess.c_str(), M_ERROR);
		return -1;
	}
	else
	{
		printClientInfo(clientAddr, stream.getSocket());
		return 0;
	}
}

void SOCKET_Connection::printServerInfo(struct addrinfo *addr)
{
	char message[200];
	strcpy(message, "[SERVER] [");
	strcat(message, getCurrentTime());
	strcat(message, "] Socket of the Server side is successfully set on: ");
	strcat(message, inet_ntoa(((struct sockaddr_in *)addr->ai_addr)->sin_addr));

	Stylist::instance()->printMessage(message, M_INFO);
}

void SOCKET_Connection::printClientInfo(struct sockaddr_storage *clientAddr, SOCKET clientSock)
{
	char ipAddr[INET6_ADDRSTRLEN];
	getIPAddrString(clientAddr, ipAddr, sizeof ipAddr);
    std::string message = "[+] [" + Str(getCurrentTime()) + "] Client connection established from: "
    		+ Str(ipAddr) + ":" + Str(getPortString(clientAddr));
    Stylist::instance()->printMessage(message.c_str(), M_WARNING);

    message.clear();
    message += Str(ipAddr) + ":" + Str(getPortString(clientAddr));
	HashMapHandler::instance()->getIpAddrHashMap()->insertNode(clientSock, message);
}


} /* namespace server */
