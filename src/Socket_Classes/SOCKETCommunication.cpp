/*
 * SOCKETCommunication.cpp
 *
 *  Created on: 7 Feb 2018
 *      Author: Fikret Basic
 */

#include "SOCKETCommunication.h"

namespace server {

SOCKET_Communication::SOCKET_Communication()
{
	this->socket = -1;
}

SOCKET_Communication::SOCKET_Communication(SOCKET s)
{
	this->socket = s;
}

SOCKET_Communication::~SOCKET_Communication()
{
	closeSocket(this->socket);
}


void SOCKET_Communication::setSocket(SOCKET s)
{
	this->socket = s;
}

SOCKET SOCKET_Communication::getSocket() const
{
	return this->socket;
}


int SOCKET_Communication::shutdownSocket(int flags)
{
	return shutdownOp(this->socket, flags);
}

int SOCKET_Communication::recv(char *buffer, size_t len, int flags)
{
	return receive(this->socket, buffer, len, flags);
}

int SOCKET_Communication::send(const char *buffer, size_t len, int flags)
{
	if (len == 0)
	{
		return sendTo(this->socket, buffer, strlen(buffer), flags);
	}
	else
	{
		return sendTo(this->socket, buffer, len, flags);
	}
}

} /* namespace server */


