/*
 * SOCKETHandler.cpp
 *
 *  Created on: 7 Feb 2018
 *      Author: its2016
 */

/**
 * \todo [DONE - 01.03.2018.] Set status messages (info, and so on, possible use of additional class...)
 */

/**
* \todo [DONE - 02.03.2018.] Handle disconnection from the Client
* (possibly make it so that Client sends one more message after it is ready to disconnect.
*/

/**
 * \todo [DONE - 02.03.2018.] Handle the error if the client disconnects from the program, while the connection still exists
 */

#include "SOCKETHandler.h"

namespace server {

SOCKET_Handler::SOCKET_Handler(SOCKET_Connection &server_socc, SOCKET_Communication &stream_comm)
	: server_socc(server_socc), stream_comm(stream_comm), sock_act(NULL) {}

// Not necessary, since the constructors of initiated objects will close down connections automatically
SOCKET_Handler::~SOCKET_Handler() {}

void SOCKET_Handler::startActivity()
{
	#if !defined (_WIN32)
		reapZombieProcesses();
	#endif

	if(this->sock_act != NULL)
	{
		this->sock_act->executeActivity(this->server_socc, this->stream_comm);
	}
}

void SOCKET_Handler::setActivity(const char* act_type)
{
	if(strcmp(act_type, "fork") == 0)
	{
	#if !defined (_WIN32)
		this->sock_act = new FORK_Action();
	#else
		this->sock_act = new THREAD_Action();
	#endif
	}
	else if(strcmp(act_type, "thread") == 0)
	{
		this->sock_act = new THREAD_Action();
	}
	else
	{
		this->sock_act = new THREAD_Action();
	}
}



} /* namespace server */



