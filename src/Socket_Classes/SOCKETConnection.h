/*
 * SOCKETConnection.h
 *
 *  Created on: 7 Feb 2018
 *      Author: its2016
 */

#include "../Includes.h"
#include "NETAddr.h"
#include "SOCKETCommunication.h"

#ifndef SOCKETCONNECTION_H_
#define SOCKETCONNECTION_H_

namespace server {

/**
 * \brief Defines the Socket Connection
 *
 * Uses the Wrapper Facade pattern to communicate with the Socket API for the socket connection services.
 */

class SOCKET_Connection {
public:

	/**
	 * \brief Empty constructor, should not be called, only for testing
	 */
	SOCKET_Connection();

	/**
	 * \brief Initialising constructor, set the particular Socket connection based on the address
	 * \param address - NET_Addr pointer to the object holding the address
	 * \param pendingConnections - limit on how many connections can wait for assignments
	 */
	SOCKET_Connection(NET_Addr *address, int pendingConnections);

	/**
	 * \brief Destructor, closes the particular Socket
	 */
	virtual ~SOCKET_Connection();

	/**
	 * \brief Forward the connection of the new Client
	 * \param stream - assign to the particular Socket communication
	 */
	int acceptClient(SOCKET_Communication &stream);

private:
	SOCKET socket;		/**< unique Socket identifier */

	/**
	 * \brief Inner function used to write out the Server IP information
	 * \param addr - server address data
	 */
	void printServerInfo(struct addrinfo *addr);

	/**
	 * \brief Inner function used to write out the Client IP information
	 * \param clientAddr - structure holding the Client data
	 * \param clientSock - client Socket identifier
	 */
	void printClientInfo(struct sockaddr_storage *clientAddr, SOCKET clientSock);
};

} /* namespace server */

#endif /* SOCKETCONNECTION_H_ */
