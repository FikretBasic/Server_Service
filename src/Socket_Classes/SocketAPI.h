/*
 * SocketAPI.h
 *
 *  Created on: 7 Feb 2018
 *      Author: its2016
 */

/**
 * \todo [DONE - 03.03.2018.] Check for libraries included if all of them pass also for Windows and does that do not, modify them:
 *
 * MutexThread			(+)
 *
 * Includes				(+)
 *
 * Guard				(+)
 *
 * Server_Service		(+)
 *
 * ThreadHandler		(+)
 *
 * Stylist				(+)
 *
 * HashMap				(+)
 *
 * HashMapHandler		(+)
 *
 * NETAddr				(+)
 *
 * SocketAPI			(+)
 *
 * SOCKETCommunication	(+)
 *
 * SOCKETConnection		(+)
 *
 * SOCKETHandler		(+)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>

#if defined(_WIN32)

#define WIN32_LEAN_AND_MEAN

#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>
#include <time.h>
#include <io.h>
#include <cstdint>

#pragma comment(lib,"ws2_32.lib") //Winsock Library

#else

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <error.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>

#endif

#define S_ERROR -1
#define SUCCESS 0

#ifndef SOCKETAPI_H_
#define SOCKETAPI_H_

/**
 * \brief Display error message and exit the application
 * \param errorMessage - error message
 */
void setErrorAndExit(const char *errorMessage);

/**
 * \brief Display error message
 * \param errorMessage - error message
 */
void setError(const char *errorMessage);

/**
 * \brief Create list of pointers which hold particular address information
 * \param addr - IP address
 * \param port - port number
 * \return struct addrinfo* - pointer to the specific list (first element)
 */
struct addrinfo* makeAddrInfo(const char *addr, const char *port);

/**
 * \brief Calls shutdown function on open sockets
 * \param flags - defines which shutdown operation to perform
 * \return int - returns the status of the function
 */
int shutdownOp(int socketID, int flags);

/**
 * \brief Receive data through the socket
 * \param socketID - particular socket identifier
 * \param buffer - character pointer which holds the received message
 * \param len - max. length of the buffer
 * \param flags - particular flags for the option
 * \return int - returns the status of the function
 */
int receive(int socketID, char *buffer, size_t len, int flags);

/**
 * \brief Send data through the socket
 * \param socketID - particular socket identifier
 * \param buffer - character pointer which holds the received message
 * \param len - max. length of the buffer
 * \param flags - particular flags for the option
 * \return int - returns the status of the function
 */
int sendTo(int socketID, const char *buffer, size_t len, int flags);

/**
 * \brief Connect a particular socket
 * \param addr - info address for the connection
 * \param errorFlag - should an error terminate the program, or not
 * \return int - status of the function
 */
int socketConnect(struct addrinfo* addr, int errorFlag);

/**
 * \brief Bind a particular socket (primarily server side)
 * \param socketID - socket identifier
 * \param addr - connection data
 * \param errorFlag - should an error terminate the program, or not
 * \return int - status of the function
 */
int socketBind(int socketID, struct addrinfo* addr, int errorFlag);

/**
 * \brief Set particular socket to listen (wait for Clients)
 * \param socketID - socket identifier
 * \param pendingConnections - number of how many connections can wait until they are handled
 * \param errorFlag - should an error terminate the program, or not
 * \return int - status of the function
 */
int socketListen(int socketID, int pendingConnections, int errorFlag);

/**
 * \brief Accept new Clients (open connection) that are queued through the listening function
 * \param socketID - socket identifier
 * \param clientAddr - client socket data, saved in a structure
 * \return int - status of the function
 */
int acceptNewClient(int socketID, struct sockaddr_storage* clientAddr);

/**
 * \brief Invoke Socket option to reuse the port
 * \param socketID - socket identifier
 * \return int - status of the function
 */
int socketOptReusePort(int socketID);

/**
 * \brief Check if the address is NULL and terminate program if it is
 * \param addr - pointer to the structure address
 * \return int - status of the function
 */
int checkAndTerminate(struct addrinfo* addr);

/**
 * \brief Get particular Socket address (independent from the IPv4 and IPv6)
 * \param sa - socket structure which holds all the necessary data
 * \return void* - returns particular type of the address
 */
void *get_in_addr(struct sockaddr *sa);

/**
 * \brief Get particular Socket port (independent from the IPv4 and IPv6)
 * \param sa - socket structure which holds all the necessary data
 * \return void* - returns particular type of the port
 */
uint16_t get_in_port(struct sockaddr *sa);

/**
 * \brief Gets the IP address in the readable character format
 * \param clientAddr- particular address structure
 * \param buffer - buffer where to store the address
 * \param length - max. length of the buffer
 */
void getIPAddrString(struct sockaddr_storage *clientAddr, char* buffer, size_t length);

/**
 * \brief Gets the Port in the readable character format
 * \param clientAddr- particular address structure
 * \return uint16_t - port number
 */
uint16_t getPortString(struct sockaddr_storage *clientAddr);

/**
 * \brief Helper function for getting read of the Zombie processes
 */
void sigchld_handler(int s);

/**
 * \brief Function for getting rid of the zombie processes
 * \return int - status of the function
 */
int reapZombieProcesses();

/**
 * \brief Close particular socket (independant from the OS)
 * \param id - Socket identifier
 */
void closeSocket(int id);

#endif /* SOCKETAPI_H_ */
