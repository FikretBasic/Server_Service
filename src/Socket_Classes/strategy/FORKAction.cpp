/*
 * FORKAction.cpp
 *
 *  Created on: 8 Mar 2018
 *      Author: its2016
 */

#include "FORKAction.h"

namespace server {

FORK_Action::FORK_Action() {}

FORK_Action::~FORK_Action() {}

void FORK_Action::handleClient(SOCKET &client_socc, const char* typeHandling)
{
#if !defined (_WIN32)
	pid_t childpid;
	SOCKET_Communication comm(client_socc);

	int recvOut;
    char buffer[1024];
    bool check = false;

	bzero(buffer, sizeof(buffer));

	if((childpid = fork()) == 0)
	{
		while(1)
		{
			std::string message;

			if (!check)
			{
				recvOut = comm.recv(buffer, 1024);

				if(strcmp(buffer, ":exit") == 0 || recvOut == 0 || recvOut == -1)
				{
					message.clear();
					message += (std::string)"[-] [" + Str(getCurrentTime()) + "] Client["
							+ HashMapHandler::instance()->getIpAddrHashMap()->get(comm.getSocket())
							+ "]: has disconnected";
					Stylist::instance()->printMessage(message.c_str(), M_ERROR);
					HashMapHandler::instance()->getIpAddrHashMap()->deleteNode(comm.getSocket());

					check = true;
				}
				else
				{
					Guard<MutexThread> guard(lockMutex);

					message.clear();
					message += (std::string)"[MESSAGE] [" + Str(getCurrentTime()) + "] Client["
							+ HashMapHandler::instance()->getIpAddrHashMap()->get(comm.getSocket())
							+ "]: " + Str(buffer);
					Stylist::instance()->printMessage(message.c_str(), M_INFO);

					comm.send(buffer, strlen(buffer));
					bzero(buffer, sizeof(buffer));
				}
			}
			else
			{
				break;
			}
		}
		exit(1);
	}

	closeSocket(client_socc);

#endif
}

} /* namespace server */
