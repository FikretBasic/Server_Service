/*
 * THREADAction.h
 *
 *  Created on: 8 Mar 2018
 *      Author: its2016
 */

#include "SOCKETActivity.h"

#ifndef SOCKET_CLASSES_STRATEGY_THREADACTION_H_
#define SOCKET_CLASSES_STRATEGY_THREADACTION_H_

namespace server {

/**
 * \brief Thread-type handling of the Client on the Server side
 *
 * One of the specific subclass of the implementation interface of the Strategy pattern
 */

class THREAD_Action : public SOCKET_Activity {
public:

	/**
	 * \brief Constructor of the class
	 */
	THREAD_Action();

	/**
	 * \brief Destructor of the class
	 */
	virtual ~THREAD_Action();

	/**
	 * \brief Handle a specific Client
	 * \param client_socc - descriptor of the specific Socket of the Client
	 */
	void handleClient(SOCKET &client_socc, const char* typeHandling = "echo");

private:

	/**
	 * \brief Thread-handler function
	 */
	static void *connection_handler(void *);

	/**
	 * \brief Thread-handler function for simulation
	 */
	static void *connection_handler_simulation(void *);
};

} /* namespace server */

#endif /* SOCKET_CLASSES_STRATEGY_THREADACTION_H_ */
