/*
 * FORKAction.h
 *
 *  Created on: 8 Mar 2018
 *      Author: its2016
 */

#include "SOCKETActivity.h"

#ifndef SOCKET_CLASSES_STRATEGY_FORKACTION_H_
#define SOCKET_CLASSES_STRATEGY_FORKACTION_H_

namespace server {

/**
 * \brief Fork-type handling of the Client on the Server side
 *
 * One of the specific subclass of the implementation interface of the Strategy pattern
 */

class FORK_Action : public SOCKET_Activity {
public:

	/**
	 * \brief Constructor of the class
	 */
	FORK_Action();

	/**
	 * \brief Destructor of the class
	 */
	virtual ~FORK_Action();

	/**
	 * \brief Handle a specific Client
	 * \param client_socc - descriptor of the specific Socket of the Client
	 */
	void handleClient(SOCKET &client_socc, const char* typeHandling = "echo");
};

} /* namespace server */

#endif /* SOCKET_CLASSES_STRATEGY_FORKACTION_H_ */
