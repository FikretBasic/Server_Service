/*
 * MessageHandler.h
 *
 *  Created on: 16 Mar 2018
 *      Author: its2016
 */

#include "../../Includes.h"

#ifndef SOCKET_CLASSES_STRATEGY_MESSAGEHANDLER_H_
#define SOCKET_CLASSES_STRATEGY_MESSAGEHANDLER_H_

class MessageHandler
{
public:

	virtual ~MessageHandler();

	/**
	 * \brief Get (or create) specific class instance
	 * \return MessageHandler* - pointer to the class instance
	 */
	static MessageHandler* instance();

	int handleClientMessage(char* message, int &instructionCounter
			, std::string &responseMessage, std::vector<std::string> &commands);

	uint16_t getConnectionCode(char *buffer);
	void resetClientCode(uint16_t code);

	void setCommands(uint16_t connectionCode, std::vector<std::string> &commands);

private:
	MessageHandler();

	static MessageHandler *instance_t;

	int checkClientCode(uint16_t code);

	std::vector<std::pair<uint16_t, bool > > clientCodes;
};

#endif /* SOCKET_CLASSES_STRATEGY_MESSAGEHANDLER_H_ */
