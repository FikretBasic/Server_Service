/*
 * SOCKETActivity.h
 *
 *  Created on: 8 Mar 2018
 *      Author: its2016
 */

#include "../../Includes.h"
#include "../../ThreadHandler.h"
#include "../../Guard.h"
#include "../../MutexThread.h"
#include "../SOCKETCommunication.h"
#include "../SOCKETConnection.h"
#include "MessageHandler.h"

#ifndef SOCKET_CLASSES_STRATEGY_SOCKETACTIVITY_H_
#define SOCKET_CLASSES_STRATEGY_SOCKETACTIVITY_H_

namespace server {

/**
 * \brief Implementor interface for the Strategy Pattern implementation class
 */

class SOCKET_Activity {
public:

	/**
	 * \brief Constructor of the class
	 */
	SOCKET_Activity();

	/**
	 * \brief Destructor of the class
	 */
	virtual ~SOCKET_Activity();

	/**
	 * \brief Main function which is called to handle a Client from the Server-side regardless of the type of the interface
	 */
	void executeActivity(SOCKET_Connection &server_socc, SOCKET_Communication &stream_comm);

private:

	/**
	 * \brief Sub-routine for handling the client
	 *
	 * Virtual function. Implemented on the side of the sub-classes
	 */
	virtual void handleClient(SOCKET &client_socc, const char* typeHandling = "echo") = 0;

};

} /* namespace server */

#endif /* SOCKET_CLASSES_STRATEGY_SOCKETACTIVITY_H_ */
