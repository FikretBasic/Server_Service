/*
 * THREADAction.cpp
 *
 *  Created on: 8 Mar 2018
 *      Author: its2016
 */

/// \todo [DONE - 19.03.2018.] Check the "Code checking" and fix it
/// \todo [DONE - 19.03.2018.] Check the simulation parameters and if they fit
/// \todo Check the wrong text output by the Server side of the Client messages

#include "THREADAction.h"

namespace server {

THREAD_Action::THREAD_Action() {}

THREAD_Action::~THREAD_Action() {}

void THREAD_Action::handleClient(SOCKET &client_socc, const char* typeHandling)
{
	if(strcmp(typeHandling, "echo") == 0)
	{
		ThreadHandler::instance()->createThread(connection_handler, (void *) &client_socc);
	}
	else
	{
		ThreadHandler::instance()->createThread(connection_handler_simulation, (void *) &client_socc);
	}
}

void *THREAD_Action::connection_handler(void *socket_desc)
{
    //Get the socket descriptor
	SOCKET sock = *(SOCKET*)(socket_desc);
	SOCKET_Communication comm(sock);

	int recvOut;
    char buffer[1024];
    bool check = false;

	bzero(buffer, sizeof(buffer));

	while(1)
	{
		std::string message;

		if (!check)
		{
			recvOut = comm.recv(buffer, 1024);

			if(strcmp(buffer, ":exit") == 0 || recvOut == 0 || recvOut == -1)
			{
				message.clear();
				message += (std::string)"[-] [" + Str(getCurrentTime()) + "] Client[" + HashMapHandler::instance()->getIpAddrHashMap()->get(comm.getSocket())
						+ "]: has disconnected";
				Stylist::instance()->printMessage(message.c_str(), M_ERROR);
				HashMapHandler::instance()->getIpAddrHashMap()->deleteNode(comm.getSocket());

				check = true;
			}
			else
			{
				Guard<MutexThread> guard(lockMutex);

				message.clear();
				message += (std::string)"[MESSAGE] [" + Str(getCurrentTime()) + "] Client["
						+ HashMapHandler::instance()->getIpAddrHashMap()->get(comm.getSocket())
						+ "]: " + Str(buffer);
				Stylist::instance()->printMessage(message.c_str(), M_INFO);

				comm.send(buffer, strlen(buffer));
				bzero(buffer, sizeof(buffer));
			}
		}
		else
		{
			break;
		}
	}

    closeSocket(sock);
    return 0;
}

void *THREAD_Action::connection_handler_simulation(void *socket_desc)
{
    //Get the socket descriptor
	SOCKET sock = *(SOCKET*)(socket_desc);
	SOCKET_Communication comm(sock);

	int recvOut;
    char buffer[1024];
    bool check = false;
    int instructionCounter;
    uint16_t connectionCode;

    std::vector<std::string> commands;

    instructionCounter = 0;
    connectionCode = 0;
	bzero(buffer, sizeof(buffer));

	while(1)
	{
		std::string message;

		if (!check)
		{
			recvOut = comm.recv(buffer, 1024);

			#if defined(_WIN32)
				Sleep(1000);
			#else
				sleep(1);
			#endif

			if(strcmp(buffer, "<t>status</t><d>connection_end</d>") == 0 || recvOut == 0 || recvOut == -1)
			{
				message.clear();
				message += (std::string)"[-] [" + Str(getCurrentTime()) + "] Client["
						+ HashMapHandler::instance()->getIpAddrHashMap()->get(comm.getSocket()) + "]: has disconnected";

				Stylist::instance()->printMessage(message.c_str(), M_ERROR);
				HashMapHandler::instance()->getIpAddrHashMap()->deleteNode(comm.getSocket());

				MessageHandler::instance()->resetClientCode(connectionCode);

				check = true;
			}
			else
			{
				Guard<MutexThread> guard(lockMutex);

				message.clear();
				message += (std::string)"[MESSAGE] [" + Str(getCurrentTime()) + "] Client["
						+ HashMapHandler::instance()->getIpAddrHashMap()->get(comm.getSocket())
						+ "]: " + Str(buffer);
				Stylist::instance()->printMessage(message.c_str(), M_INFO);

				message.clear();

				if (MessageHandler::instance()->handleClientMessage(buffer, instructionCounter, message, commands))
				{
					if(message.compare("<t>status</t><d>code_passed</d>") == 0)
					{
						connectionCode = MessageHandler::instance()->getConnectionCode(buffer);
						MessageHandler::instance()->setCommands(connectionCode, commands);
					}

					memset(buffer, '\0', sizeof(buffer));
					strcpy(buffer, message.c_str());
				}
				else
				{
					memset(buffer, '\0', sizeof(buffer));
					strcpy(buffer, "<t>status</t><d>message_error</d>");
				}

				comm.send(buffer, strlen(buffer));
				bzero(buffer, sizeof(buffer));
			}
		}
		else
		{
			break;
		}
	}

    closeSocket(sock);
    return 0;
}


} /* namespace server */
