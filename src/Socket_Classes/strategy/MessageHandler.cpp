/*
 * MessageHandler.cpp
 *
 *  Created on: 16 Mar 2018
 *      Author: its2016
 */

#include "MessageHandler.h"

MessageHandler* MessageHandler::instance_t = 0;

MessageHandler::MessageHandler()
{
	clientCodes.push_back(std::make_pair(1231, false));
	clientCodes.push_back(std::make_pair(1232, false));
	clientCodes.push_back(std::make_pair(1233, false));
	clientCodes.push_back(std::make_pair(1234, false));

	clientCodes.push_back(std::make_pair(2231, false));
	clientCodes.push_back(std::make_pair(2232, false));
	clientCodes.push_back(std::make_pair(2233, false));
	clientCodes.push_back(std::make_pair(2234, false));
}

MessageHandler::~MessageHandler()
{
	delete this->instance_t;
}


MessageHandler* MessageHandler::instance()
{
    if (!instance_t)
    	instance_t = new MessageHandler();
    return instance_t;
}

int MessageHandler::handleClientMessage(char* message, int &instructionCounter
		, std::string &responseMessage, std::vector<std::string> &commands)
{
	SimplePacket packet(message);

	switch(instructionCounter)
	{
		case 0:
			if(packet.getType().compare("status") == 0 && packet.getData().compare("ready") == 0)
			{
				responseMessage = "<t>status</t><d>code_request</d>";
				instructionCounter++;

				return 1;
			}
			break;
		case 1:
			if(packet.getType().compare("code") == 0)
			{
				if(checkClientCode((uint16_t)std::atol(packet.getData().c_str())) > 0)
				{
					responseMessage = "<t>status</t><d>code_passed</d>";
					instructionCounter++;

					return 1;
				}
				else
				{
					responseMessage = "<t>status</t><d>code_failed</d>";
					return 1;
				}
			}
			break;
		case 2:
			if(packet.getType().compare("status") == 0 && packet.getData().compare("ready") == 0)
			{
				if (commands.size() > 0)
				{
					responseMessage = "<t>command</t><d>" + commands.back() + "</d>";
					commands.pop_back();
				}
				else
				{
					responseMessage = "<t>status</t><d>waiting</d>";
					instructionCounter++;
				}
			}
			else if (packet.getType().compare("status") == 0 && packet.getData().compare("sim_started") == 0)
			{
				responseMessage = "<t>status</t><d>sim_status</d>";
			}
			else if (packet.getType().compare("status") == 0 && packet.getData().compare("sim_finished") == 0)
			{
				responseMessage = "<t>status</t><d>sim_received</d>";
			}
			else
			{
				responseMessage = "<t>status</t><d>waiting</d>";
			}

			return 1;

			break;
		case 3:
			responseMessage = "<t>command</t><d><RUN>0<\\RUN></d>";
			return 1;

			break;
		default:
			return -1;
	}

	return -1;
}


int MessageHandler::checkClientCode(uint16_t code)
{
	for (size_t i = 0; i < clientCodes.size(); i++)
	{
		if (clientCodes.at(i).first == code)
		{
			if (clientCodes.at(i).second)
			{
				return -1;
			}
			else
			{
				clientCodes.at(i).second = true;
				return 1;
			}
		}
	}

	return -1;
}

void MessageHandler::resetClientCode(uint16_t code)
{
	for (size_t i = 0; i < clientCodes.size(); i++)
	{
		if (clientCodes.at(i).first == code)
		{
			clientCodes.at(i).second = false;
		}
	}
}

uint16_t MessageHandler::getConnectionCode(char *buffer)
{
	SimplePacket packet(buffer);
	return (uint16_t)std::atol(packet.getData().c_str());
}

void MessageHandler::setCommands(uint16_t connectionCode, std::vector<std::string> &commands)
{
	uint8_t leadingVal = connectionCode / 1000;

	if (leadingVal == 1)
	{
		commands.push_back("<RUN>0<\\RUN>");
		commands.push_back("<RUN>1<\\RUN>");
		commands.push_back("<TU>NS<\\TU>");
		commands.push_back("<TS>100<\\TS>");
	}
	else if (leadingVal == 2)
	{
		commands.push_back("<TS>0<\\TS><TU>FS<\\TU><RUN>0<\\RUN><TR>1<\\TR>");
		commands.push_back("<E>6<\\E><RUN>1<\\RUN>");
		commands.push_back("<CMD>0001A400000100010200E10103012B016001<\\CMD><E>5<\\E><RUN>1<\\RUN>");
		commands.push_back("<TU>MS<\\TU><TS>1<\\TS>");
	}
	// Add additional lines for additional simulations
}
