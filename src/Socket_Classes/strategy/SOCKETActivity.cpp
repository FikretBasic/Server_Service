/*
 * SOCKETActivity.cpp
 *
 *  Created on: 8 Mar 2018
 *      Author: its2016
 */

#include "SOCKETActivity.h"

namespace server {

SOCKET_Activity::SOCKET_Activity() {}

SOCKET_Activity::~SOCKET_Activity() {}

void SOCKET_Activity::executeActivity(SOCKET_Connection &server_socc, SOCKET_Communication &stream_comm)
{
	while(1)
	{
		Stylist::instance()->printMessage("[SERVER] Waiting for Client...", M_WARNING);
		server_socc.acceptClient(stream_comm);
		SOCKET client_socc = stream_comm.getSocket();

		handleClient(client_socc, "thread");
	}
}



} /* namespace server */
