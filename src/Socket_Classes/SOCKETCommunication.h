/*
 * SOCKETCommunication.h
 *
 *  Created on: 7 Feb 2018
 *      Author: its2016
 */

#include "../Includes.h"

#ifndef SOCKETCOMMUNICATION_H_
#define SOCKETCOMMUNICATION_H_

namespace server {

/**
 * \brief Enables setting and controlling the Socket communication
 *
 * Defines sending and receiving data through the Socket by using Wrapper Facade pattern to communicate with the Socket API.
 */

class SOCKET_Communication
{
public:

	/**
	 * \brief Empty constructor, should not be called unless for testing and sub-step
	 */
	SOCKET_Communication();

	/**
	 * \brief Initialising constructor, sets the particular socket identifier
	 * \param s - socket identifier
	 */
	SOCKET_Communication(SOCKET s);

	/**
	 * \brief Destructor, closses the saves socket
	 */
	virtual ~SOCKET_Communication();

	/**
	 * \brief Set particular socket identifier (change with the current one)
	 * \param s - socket identifier
	 */
	void setSocket(SOCKET s);

	/**
	* \brief Get saved Socket
	* \return SOCKET - socket identifier
	*/
	SOCKET getSocket() const;

	/**
	 * \brief Shutdown the saved socket based on the flag
	 * \param flags - flag which describes the shutdown procedure
	 */
	int shutdownSocket(int flags = 0);

	/**
	 * \brief Receive data at the other end of the socket
	 * \param buffer - char pointer to the received data
	 * \param len - maximum length of the received data
	 * \param flags - option descriptors
	 * \return int - status of the function
	 */
	int recv(char *buffer, size_t len, int flags = 0);

	/**
	 * \brief Send data at the other end of the socket
	 * \param buffer - char pointer to the sent data
	 * \param len - maximum length of the sent data
	 * \param flags - option descriptors
	 * \return int - status of the function
	 */
	int send(const char *buffer, size_t len = 0, int flags = 0);

private:

	SOCKET socket;		/**< unique socket identifier */
};

} /* namespace server */

#endif /* SOCKETCOMMUNICATION_H_ */
