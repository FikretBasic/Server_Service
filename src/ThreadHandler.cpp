/*
 * ThreadHandler.cpp
 *
 *  Created on: 21 Feb 2018
 *      Author: its2016
 */

#include "ThreadHandler.h"

ThreadHandler* ThreadHandler::instance_t = 0;

ThreadHandler::ThreadHandler() {}

ThreadHandler::~ThreadHandler()
{
	delete this->instance_t;
}

#if defined(__linux__) || defined(_WIN32)

int ThreadHandler::createThread(void *(*start_routine) (void *), void *arg,
		pthread_t *thread, const pthread_attr_t *attr)
{
	pthread_t t;
	if (thread == 0)
		thread = &t;

	int status = pthread_create( thread , attr ,  start_routine , arg );

    if( status < 0)
    {
        printf("Error creating thread!");
        return -1;
    }
    return status;
}


#elif defined(__sun)

int ThreadHandler::createThread(void *(*entry_point) (void *), void *arg,
		 long flags, thread_t *thread, long stack_size,
		 void *stack_pointer)
{
	thread_t t;
	if (t_id == 0)
		t_id = &t;
	return thr_create (stack_size, stack_pointer, entry_point, arg, flags, t_id);
}

#endif

ThreadHandler* ThreadHandler::instance()
{
    if (!instance_t)
    	instance_t = new ThreadHandler();
    return instance_t;
}

