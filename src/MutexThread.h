/*
 * MutexThread.h
 *
 *  Created on: 7 Feb 2018
 *      Author: its2016
 */

#include "Includes.h"

#ifndef MUTEXTHREAD_H_
#define MUTEXTHREAD_H_

/**
 * \brief Multi-Platform Mutex class for manipulation with threads
 *
 * Mutex class made primarily with intention of enabling multi-platform options
 * for acquiring and releasing a lock. \n It has the possibility of extension for additional functions. \n
 * Currently it supports Linux, Windows and some other Unix systems (primarily Solaris).
 */
class MutexThread
{
public:
	/**
	 * \brief Constructor of the Mutex Thread class
	 *
	 * Initialises the lock.
	 */
	MutexThread();

	/**
	 * \brief Destructor of the Mutex Thread class
	 *
	 * Destroys the lock.
	 */
	virtual ~MutexThread();

	/**
	 * \brief Acquire the lock
	 * \return int - returns the status of the locking
	 */
	int acquire();

	/**
	 * \brief Release the lock
	 * \return int - returns the status of the de-locking
	 */
	int release();

private:

#if defined(__linux__)
	pthread_mutex_t lock;		/**< \brief Variable used as a lock for Mutex control */
#elif defined(_WIN32)
	CRITICAL_SECTION lock;		/**< \brief Variable used as a lock for Mutex control */
#elif defined(__sun)
	mutex_t lock;				/**< \brief Variable used as a lock for Mutex control */
#else
	#error No support OS: Mutex locking
#endif

	/**
	 * \brief Initialisation Constructor of the Mutex class
	 *
	 * Put in the private scope, to shield from the possibility of copying a Mutex lock.
	 */
	MutexThread(const MutexThread &);

	/**
	 * \brief Overloaded operator of equality of the Mutex class
	 *
	 * Put in the private scope, to shield from the possibility of copying a Mutex lock.
	 */
	void operator=(const MutexThread &);
};


#endif /* MUTEXTHREAD_H_ */
