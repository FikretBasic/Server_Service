/*
 * Documentation.h
 *
 *  Created on: 9 Mar 2018
 *      Author: its2016
 */

/**
 * Documentation file. Used for defining front page text and global elements.
 */

#ifndef DOCUMENTATION_H_
#define DOCUMENTATION_H_

/**
 *
 *	\mainpage Server application for handling Multiple Client interactions
 *
 *	\author Fikret Basic
 *	\version 1.0
 *	\date 25.02.2018.
 *
 *	\warning If used with \b Windows Opearating System, please ensure that your project is linked
 *	with the POSIX Pthread library for Windows. <a> https://www.sourceware.org/pthreads-win32/ </a>
 *
 *	\warning Due to the complicated nature, Windows version only \b supports threads as the Client interaction method.
 *
 *	\copyright Technical University of Graz - All Rights Reserved
 *
 *	\tableofcontents
 *
 *	\section intent_sec Overview of the Application
 *
 *	The Application was developed in Eclipse both for Windows and Linux operating system. \n \n
 *	The main purpose of the Application is to serve as a Server-Client provider for the SystemC multi-simulations,
 *	by providing input commands (e.g. Gazebo, but should be independent from the Server use). The Server provider should also be
 *	Independent from the purpose (taken care with Design Patterns). \n
 *	More information can be found here \ref overviewPage .
 *
 *	\section compile_sec Running the Application
 *
 *	The Application can be run both on the Windows and Linux system. It is only necessary to compile it. \n
 *	After that in "Debug" folder lies the executable or the binary file which can then be run though the \b terminal command. \n
 *	The program accepts up to three arguments:
 *	- \b arg1 : the server address
 *	- \b arg2 : the connection port
 *	- \b arg3 : type of the client handling
 *
 *	If no argument is given, the default one will be used (for the address 127.0.0.1, and port 8888). \n
 *	"Client handling" can be "fork" and "thread". It takes "thread" as the default. Windows only supports "thread".
 *
 *	For the \b Windows execution, it is recommended to take and include the \i src files in a new Visual Studio Project,
 *	since it is the only environment where the system was tested and is to be expected to work using it.
 *
 *	\section design_sec Processing of the Application
 *
 *	The program works with conjunction of the client program, where the execution follows the following steps:
 *	-# Server starts, registers its address and port
 *	-# Server enters a standby phase, awaiting the client
 *	-# A new Client connects to the Server, Server registers it, assigns a separate thread or process, and goes back to step 2.
 *
 *	The thread connection between the Server and Client follows the following idea:
 *	-# Connection is established
 *	-# Communication:
 *		-# Send data
 *		-# Receive data
 *	-# Terminate connection
 *
 *	\image html Windows_Output.png "Server communication with two Clients on Windows" width=2cm
 *
 *	\section patterns_sec Principles and Patterns
 *
 *	This section gives a brief overview over the Design Patterns that were used. \n
 *	For a more detailed list please check \ref designPatternPage.
 *
 *	The program was designed and implemented using heavily the \b Wrapper \b Facade design pattern. \n
 *	This pattern hides the \b C language \b<Socket API> from the user by constructing individual classes. \n
 *	All classes related to the Socket communication are included inside the \b<Socket_Classes> folder.
 *
 *	Wrapper Facade was also used to enable certain classes to be \b independent from the operating system in use. \n
 *	The example are the \b Thread and \Mutex classes which provide functions that are independent on the programming platform.
 *
 *	\b Singleton was also heavily used, primarily for enabling the use of the global functionality of service and tools (like the \n
 *	\b Stylist class used to give coloring features in the terminal status window).
 *
 *	\b Scoped \b Locking was also used in conjunction with the \b Guard class for easier maintenance of the Mutex allocations.
 *
 *	Currently in development is the use of the \b strategy \b pattern or a similar one for the enabling the use and testing of different
 *	Server handling of the multiple clients.
 *
 *	\section workflow_sec Design Directions
 *
 *	This section will briefly explain the elements that are done and that are to be done.
 *
 *	\subsection done_subsec Done List
 *
 *	- Set an initial Server and Multi-Client system based on an echo functionality
 *	- Separate completely the Socket API functionalities (C functions) from the Socket interface (classes)
 *	- Implement a more stylised and structured terminal status display
 *	- Add Thread handling to the Server execution, alongside the Mutex for a more synchronised request handling
 *	- Add Guard scoped locking handling
 *	- Revise the program to work both under the Linux and Windows operating system (and partially with other Unix, primarily Solaris)
 *	- Set an initial documentation page (Doxygen)
 *	- Extend the documentation Doxygen page.
 *	- Finish the implementation of the Strategy pattern for different server-client handling
 *
 *	\subsection tobe_subsec To be done List
 *
 *	- Replace the "Client" echo functionality with the testing SystemC simulation
 *	- Replace the "Server" echo functionality with the command simulation handling (based on Gazebo-style commands)
 *	- Provide a sufficient interface (potentially through design pattern) to include different Client and Server functionalities
 *
 *	\section reference_sec References
 *
 *	[1] <a> http://beej.us/guide/bgnet/html/multi/index.html </a> , 08.03.2018. \n
 *	[2] Paul O' Steen, Transitioning from UNIX to Windows Socket Programming \n
 *	[3] Douglas C. Schmidt, Wrapper Facade - A Structural pattern for encapsulating Functions within Classes, C++ Report Magazine, 1999. \n
 *	[4] ... TO ADD OTHERS ...
 */

/**
 * \page overviewPage Additional Application Information
 *
 * \section firstSec Section part
 *
 * TO BE DONE...
 */

/**
 * \page designPatternPage Design Pattern Use and Explanation
 *
 * \section secDirection Direction of the Development
 *
 * TO BE DONE...
 */



/**
 *	\namespace server
 *	\brief Namespace used for identification of the SOCKET type classes and elements
 */

#endif /* DOCUMENTATION_H_ */
