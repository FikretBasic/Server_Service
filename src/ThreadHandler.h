/*
 * ThreadHandler.h
 *
 *  Created on: 21 Feb 2018
 *      Author: its2016
 */

#ifndef THREADHANDLER_H_
#define THREADHANDLER_H_

#include "Includes.h"

/**
 * \brief Class for handling different systems for threads
 *
 * Uses Wrapper Facade design pattern for multi-platfrom handling (Linux, Windows and Unix - Solaris). \n
 * Uses Singleton design pattern for global control and limitation of one instance.
 */

class ThreadHandler {
public:

	/**
	 * \brief Thread Handler destructor
	 *
	 * It deletes (frees) the instance of the ThreadHandler
	 */
	virtual ~ThreadHandler();

#if defined(__linux__) || defined(_WIN32)
	/**
	 * \brief Create a new thread
	 * \param *start_routine - specific function to handle the thread
	 * \param arg - arguments sent to the function
	 * \param thread - create a specific thread, does not have to be specified
	 * \param attr - define thread attributes, do not have to be specified
	 * \return int - status of the function execution
	 */

	int createThread(void *(*start_routine) (void *), void *arg,
			pthread_t *thread = 0, const pthread_attr_t *attr = NULL);
#elif defined(__sun)
	int createThread(void *(*entry_point) (void *), void *arg,
			 long flags = 0, thread_t *thread = 0, long stack_size = 0,
			 void *stack_pointer = 0);
#endif

	/**
	 * \brief Singleton handling of the class instance
	 * \return ThreadHanlder - return the single instance of the class
	 */
	static ThreadHandler *instance();

private:
	ThreadHandler();	/**< \brief Empty constructor which is only called inside the class */

	static ThreadHandler *instance_t;	/**< \brief single instance of the Class */
};

#endif /* THREADHANDLER_H_ */
