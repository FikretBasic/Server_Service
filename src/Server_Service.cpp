//============================================================================
// Name        : Server_Service.cpp
// Author      : Fikret
// Version     :
// Copyright   : &copy Fikret Basic - TU Graz All Rights Reserved
// Description : Hello World in C++, Ansi-style
//============================================================================



/// \todo [DONE - 02.03.2018.] Add the option when running the program to add arguments for the hostname and port number
/// \todo [DONE - 08.03.2018.] Reconfigure the standard output to also include the current timestamp of the message

#include "Socket_Classes/NETAddr.h"
#include "Socket_Classes/SOCKETConnection.h"
#include "Socket_Classes/SOCKETCommunication.h"
#include "Socket_Classes/SOCKETHandler.h"
#include "Includes.h"

#if defined(_WIN32)
/**
* \brief Initial setup for start of the Socket communication for Windows
*/
void initializeSocketWin();
#endif

int main(int argc, const char* argv[])
{
	std::cout << "\n\t\t\t>>> Server Initialisation <<<" << std::endl << std::endl;

	const char* hostaddr;
	const char* port;
	const char* activity;

	if (argc == 1)
	{
		hostaddr = NULL;
		port = "8888";
		activity = "thread";
	}
	else if (argc == 2)
	{
		hostaddr = argv[1];
		port = "8888";
		activity = "thread";
	}
	else if (argc == 3)
	{
		hostaddr = argv[1];
		port = argv[2];
		activity = "thread";
	}
	else if (argc == 4)
	{
		hostaddr = argv[1];
		port = argv[2];
		activity = argv[3];
	}
	else
	{
		Stylist::instance()->printMessage("[STATUS] Incorrect use of the program -- Please follow this guideline:\n"
				"(*) Use the program WITHOUT the arguments (local host address is used with port 8888, with activity set to \'thread\')\n"
				"(*) Use the program WITH 1. (host address) or/and 2. (port number) argument or/and 3. (activity type) argument", M_ERROR);

		exit(1);
	}

	#if defined(_WIN32)
		initializeSocketWin();
	#endif

	// Internet address of server.
	server::NET_Addr *addr = new server::NET_Addr(hostaddr, port);
	server::SOCKET_Connection server(addr, 10);
	server::SOCKET_Communication sockStream;

	server::SOCKET_Handler socketProcess(server, sockStream);
	socketProcess.setActivity(activity);
	socketProcess.startActivity();

	delete addr;

	return 0;
}

#if defined(_WIN32)
void initializeSocketWin()
{
	WSADATA wsaData;
	int res;

	// Initialize Winsock
	res = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (res != 0) {
		printf("WSAStartup failed with error: %d\n", res);
		exit(1);
	}
}
#endif
